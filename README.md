# README #

Este repositório deve servir como fonte de consulta para implementações feitas pelos autores. Refatorações para as soluções 
aqui versionadas são bem vindas

### Fluxo de contribuição ###

* Cada desenvolvedor cria uma branch feature com base na branch develop
* Quando for solicitar alguma modificação na branch pai é necessário atualizar sua branch (feature) com a branch develop e 
  por fim enviar um pull request para a branch develop marcando o dev que vai fazer a revisão
